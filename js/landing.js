document.addEventListener("DOMContentLoaded", init);

function init() {
    cb = document.getElementById("continueBox");

    cb.addEventListener("click", function () {
        if (cb.clientWidth < 400) {
            window.location.href = "index.html";
        }
    });

    cb.addEventListener("touchstart", function () {
        boxHighlight();
    });

    cb.addEventListener("touchend", function () {
        boxNormal();
    });

    cb.addEventListener("touchcancel", function () {
        boxNormal();
    });

   /* let ct = $("#continueText");

    ct.addEventListener("click", function () {
        window.location.href = "index.html";
    })
*/
}

function boxHighlight() {
    if (cb.clientWidth < 500) {
        cb.style.backgroundColor = "#6622FF";
        cb.style.color = "#DDDDDD";
    }
}

function boxNormal() {
    if (cb.clientWidth < 500) {
        cb.style.backgroundColor = "#6666FF";
        cb.style.color = "#000000";
    }
}

