document.addEventListener("DOMContentLoaded", init);

function init() {
    let name = window.location.search.substring(1);
    document.getElementById("title").innerHTML = name;
    document.getElementById("heading").innerHTML = "Willkommen zur " + name + " Seite";
}