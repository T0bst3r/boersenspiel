document.addEventListener("DOMContentLoaded", init);
let cb;
let graph;
let ticker;
let start;
let buyButton;
let sellButton;
let running = false;
let course = [10000,];
let streak = 0;
let wallet = 100000;
let shares = 0;

function init() {
    cb = document.getElementById("graph");
    graph = cb.getContext("2d");
    cb.width = 1200;
    cb.height = 400;
    graph.imageSmoothingEnabled = false;
    start = document.getElementById("start");
    start.addEventListener("click", startClick);
    buyButton = document.getElementById("buy");
    buyButton.addEventListener("click", function () {
        buy(1)
    });
    sellButton = document.getElementById("sell");
    sellButton.addEventListener("click", function () {
        sell(1)
    });
}

function startClick() {
    if (running) {
        onStop();
    } else {
        onStart();
    }
    running = !running;

}

function onStart() {
    course = [10000,];
    streak = 0;
    wallet = 100000;
    shares = 0;
    document.getElementById("market").innerHTML = "";
    document.getElementById("course").innerHTML = "";
    updateDisplay(10000);
    start.innerHTML = "stop";
    ticker = setInterval(function () {
        plot(calculateCourse());
    }, 100);
}

function onStop() {
    start.innerHTML = "restart";
    clearInterval(ticker);
    sell(shares);
}

function plot(course) {

    //let debug = document.getElementById("debug");
    graph.clearRect(0, 0, cb.width, cb.height);
    graph.beginPath();
    graph.moveTo(0, cb.height - 2 * course[0] / 100);
    for (let i = 0; i < course.length; i++) {
        //debug.innerHTML = "Plotting Line to " + 4 * i + " / " + cb.height + " - " + course[i]/100;
        graph.lineTo(4 * i, cb.height - 2 * course[i] / 100);
    }
    graph.stroke();
}

function calculateCourse() {
    if (course.length > 300) onStop();

    let market = document.getElementById("market");
    let courseValue = document.getElementById("course");
    let sharesV = document.getElementById("sharesV");

    let factor = Math.random();
    if (streak >= 3) {
        factor = factor >= 0.25 ? 1 : -1;
        market.innerHTML = "Bull " + streak;
    } else if (streak <= -3) {
        factor = factor >= 0.75 ? 1 : -1;
        market.innerHTML = "Bear " + streak;
    } else {
        factor = factor >= 0.5 ? 1 : -1;
        market.innerHTML = "None " + streak;
    }

    if (factor > 0 && streak >= 0 || factor < 0 && streak <= 0) {
        streak += factor;
    } else {
        streak = factor;
    }

    let delta = Math.floor((Math.random() * 81) + 10); //Factor is 81 because Random is exclusive 1 and we are cutting off digits. Also calculating in Cents.
    let currentCourse = course[course.length - 1] + delta * factor;
    if (currentCourse < 0) currentCourse = 0;

    courseValue.innerHTML = (currentCourse / 100).toFixed(2);
    course.push(currentCourse);
    sharesV.innerHTML = "" + (shares * currentCourse / 100).toFixed(2);
    //if (course.length > 200) course.shift();
    return course;
}

function buy(amount) {
    let currentCourse = course[course.length - 1];
    if (wallet >= currentCourse * amount) {
        wallet -= currentCourse * amount;
        shares += amount;
    }
    updateDisplay(currentCourse);
}

function sell(amount) {
    if (shares < amount) return;
    let currentCourse = course[course.length - 1];
    wallet += currentCourse * amount;
    shares -= amount;
    updateDisplay(currentCourse);
}

function updateDisplay(currentCourse) {
    let sharesA = document.getElementById("sharesA");
    let sharesV = document.getElementById("sharesV");
    let w = document.getElementById("wallet");

    w.innerHTML = (wallet / 100).toFixed(2);
    sharesA.innerHTML = "" + shares;
    sharesV.innerHTML = "" + (shares * currentCourse / 100).toFixed(2);
}